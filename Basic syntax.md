# Syntax

## Comments

Anything after __#__ is a comment and will be skipped by the interpreter. Comment as much as you can see fit, BASH scripts can be very misleading and not-so-nice to look at (especially after a long time).

```bash
# this is a comment

# and this is also a comment 
```

<br/>

## Variables

### Declaration

You can declare variable as simply as this:
```bash
name="John"
```
or
```bash
name=John
```
As long as there are no white-spaces both options are completly valid. If you want to have white-spaces in your data, it is much easier to use the first option with double quotations.
<br/>

#### Readonly variables/Constants
Using __readonly__ keyword you can declare a constant variable.
```bash
num=4
readonly num
echo $num

# num=44 <-- this will trigger error
```
<br/>

#### Local variables

Using __local__ keyword; only use this within the function scope (the variable _name_ will not be accessible outside of function scope)
```bash
function call_name() {
	local name=John
	echo $name
}
```
<br/>

### Reading Data from Variables

When you want to read data from your variable, just put __$__ in front of it.

```bash
echo "My name is $name"
```
or
```bash
echo "My name is ${name}"
```
<br/>

### Concatenating Variables

You don't need to put any operators in between variables, simply just read data from it and put them in desired sequence.

```bash
greet="Greetings!"
name=John
echo "$greet $name"
```

__NOTE:__
You do not have to use double quotations when combining variables, however it ensures right formatting and it is just nice habbit. 

Do NOT use single quotations; single quotations do not allow reading variable data.
<br/>

### Variable Types

In BASH there 2 types of variables:

* __System variables__
System or Global variables are stored within the system and are available at any moment. Here's a few common examples and honorable mentions. To view all system variables use `$ env` or `$ set`.

	* _BASH_ - path to BASH interpreter
	* _BASH_VERSION_ - BASH version
	* _DESKTOP_SESSION_ - Desktop enviroment
	* _EDITOR_ - default text editor
	* _GROUPS_ - group ID
	* _HISTFILE_ - BASH history file
	* _HOME_ - home directory for current user
	* _LANG_ - current language settings
	* _LOGNAME_ - login username
	* _MACHTYPE_ - device architecture
	* _PWD_ - current working directory
	* _SHELL_ - path to login shell
	* _TERM_ - login terminal type
	* _UID_ - user ID
	* _USER_ - current user name
	* _USERNAME_ - current username

<br/>

* __Local variables__
Local variables are stored only in the script/program, hence they can not be called within the shell. 
Do not name variables with the same name as system variables. Doing that, will overide system variable and system can become unstable!
