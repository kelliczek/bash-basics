# Functions

## Readonly variables & functions:
	-functions
	function hello() {
		echo "hello world"
	}
	readonly -f hello


## Declaring function

```sh
function NAME() {
	commands
}
```

## Calling function

```sh
NAME
```

## Passing arguments to a function

```sh
function NAME() {
	echo $1 #same as passing arguments to a script
}
```

## Using local variables in functions

```sh
function NAME() {
	local VARIABLE = "TIM" #it can be only accessed within the method
}
```

## Returning functions

```sh
function NAME() {
	local file=$1
	if [ -f $file ]
	then
		return True
	else
		return False
	fi
}
```

## Example

```sh
function check_for_input_arguments() {
	if [ $# -eq 0 ]
	then
		echo "please provide a valid argument"
		exit
	else
		...commands
	fi
}
```

```sh
# functions with positional parameters

function greet() {
	echo "Hello, my name is $1 and $2 years old"
}

greet "John" "42"
```

