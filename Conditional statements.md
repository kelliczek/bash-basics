# Conditional statements

## Comparison operators

```sh
val1 -eq = == val2 # true if values are equal
```
```sh
val1 -ne != val2 #true if values are not equal
```
```sh
val1 -gt > - val2 #true if val1 is greater than val2
```
```sh
val1 -ge >= val2 #true if val1 is greater or equal than val2
```
```sh
val1 -lt < - val2 #true if val1 is less than val2
```
```sh
val1 -le <= val2 #true if val1 is less or equal than val2
```


<br>


## Logical Operators

### AND

```sh
if [ "$VAR1" -eq "$VAR2" ] && [ "$VAR1" -ne "$VAR2" ]
# Logical AND &&   or   [ statement1 -a statement2 ]
```

### OR
```sh
if [ "$VAR1" -eq "$VAR2" ] || [ "$VAR1" -ne "$VAR2" ]
# Logical OR ||   or   [ statement1 -o statement2 ]
```


### Examples

#### Integer

```sh
if [ "$a" -eq "$b" ] #square brackets if using comparison argument; -eq -ne -gt -ge -lt -le

if (("$a" == "$b")) #double brackets if using regular math compare operator; same operators like in .NET languages
```


#### String

```sh
if [ "$a" -eq "$b" ] #square brackets if using comparison argument; -eq -ne -gt -ge -lt -le -z (= string is null, length is 0)

if [[ "$a" == "$b" ]] #double square brackets if using regular math compare operator; same operators like in .NET languages
```

<br>


## File operands

### Checking file existence

```sh
if [ -e $FILENAME ]
```

### Checking if it is a file

```sh
if [ -f $FILENAME ]
```

### Checking if the file is directory

```sh
if [ -d $FILENAME ]
```

### Checking if it's text file

```sh
if [ -c $FILENAME ]
```

### Checking if it's picture/video

```sh
if [ -b $FILENAME ]
```


### Checking if the file is NOT empty
```sh
if [ -s $FILENAME ]
```

### Checking if the file is readable

```sh
if [ -r $FILENAME ]
```

### Checking if the file is executable

```sh
if [ -x $FILENAME ]
```

### Checking if the file is writable

```sh
if [ -w $FILENAME ]
```

<br>


## IFs

### IF statement

```sh
if [ condition ]
then
	statement
fi
```


<br>


### IF-ELSE statement

```sh
if [ condition ]
then
	statement
else
	statement
fi
```


<br>


### IF-ELSE-ELSE statement

```sh
if [ condition ]
then
	statement
elif [ condition ]
then
	statement
else
	statement
fi
```

<br>

## Case

```sh
case expression in 
	pattern1 )
		statements ; ;
	pattern2 )
		statements ; ;
	. . .
esac
```

### Regular expressions in case statements

```sh
read -p "Enter some character: " value

case $vehicle in
	[a-z] ) #expecting small single character from a to z
		echo "$value character in a...z" ;;
	[A-Z] ) #patern for capital A to Z; if doesn't work terminal --> LANG=C #this is for terminal settings
		echo "$value character in A...Z" ;;
	[0-9] ) #patern for numbers
		echo "$value character in 0...9" ;;
	? ) #patern for single special character
		echo "$value character is a special character" ;;
	* ) #default
		echo "$value character is unknown" ;;
esac
```

### Example

```sh
#user input ==4 or ==10 or anything
int=$1
case $int in
	"4" )
		echo "something something" ;;
	"10" )
		echo "something something" ;;
	*  )
		echo "something something" ;;
esac
```


```sh
read -p "Are you ok? Y/N" ANSWER
case "$ANSWER" in
	[yY] | [yY][eE][sS])	#yep, ) is required
		echo "That is good"
		;;
	[nN] | [nN][oO])
		echo "To bad"
		;;
	*)
		echo "Please enter valid option"	#if the user won't provided Y/N (default if unsupported input)
		;;
esac
```