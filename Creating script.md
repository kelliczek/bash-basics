# BASH - basic scripting

A quick guide how to simply make BASH scripts with tips and tricks.

## Writing script/program

### 1. Declare shebang

This should be the first line of the script. Following shebang (#!) is the path to the interpreter that should be used to interpret remaining lines of code.

Formatting is also important. The shebang must be on the first line of the file. There must be no spaces before the _#_ or between the _!_ and the path to the interpreter.

In most cases the first line of the code is going to be __#!/bin/bash__. However you can always double-check path to the interpreter using `$ echo $BASH` command.
<br/>

### 2. Script/Program description

It is not required, but very useful to quickly find out what the script/program does. Anything after __#__ is a comment and will be skipped by the interpreter.
<br/>


### 3. Set BASH to exit on error

It is also not required, however nice to include in the script `set -e`. When this option is on, when any command fails, the shell immediately shall exit.
<br/>

### 4. Set global variables

Making global variables may not be necessary in all scripts/programs, however if needed the best place to put them on the top of the file (right bellow `set -e`). This way eliminates searching through out the script.
<br/>

### 5. Make functions

It makes your code easier to read and (potentionally) run smoothly. One way to have a cleaner codebase is to use the DRY (Don’t Repeat Yourself) programming principle. It is aimed at reducing repetition of information of all kinds.

Also it is much easier to call a function rather than rewriting the entire function body every time it is needed.
<br/>

### 6. Validation

Have some kind of validation logic inside your script/program especially for user input, checking for valid file paths, empty files and so on. The benefit of this is much improved usability and clarity.
<br/>

### 7. Make file executable

In order to run your script/program, it is essential to make it executable. Simply add executable file permission to the file using `$ chmod +x file_name.sh` or using octal numbers.
<br/>

## Executions

Execution or in other words running the script/program. There are 2 common ways how to run the file.

* __Simple way__
This way system will choose its own default interpreter or the one provided in the file: `$ ./file_name.sh` 
<br/>

* __Explicit way__
This way provides interpreter; system doesn't have to look it up in the file: `$ bash file_name.sh`
<br/>


## Pasing arguments

```sh
bash SCRIPT.SH argument1 argument2 ... argumentN
# in script argument are assigned $1 $2 ... $n ($0 returns the name of the string)
```

```sh
# passing argumetns to the array
argument_variable=("$@")
	# you can then access the values (arrays index from 0)
	${argument_variable[0]} ${argument_variable[1]} ... ${argument_variable[N]}

	# printing all arguments
	echo $@

	# printing number of passing arguments
	echo $#
```

## Sample scripts/programs

* ### First script 
```bash
#!/bin/bash
# Small sample script that prints 'Hello world!' on the screen

set -e

echo "Hello world!"
```
<br/>

