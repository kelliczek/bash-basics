# Arithmetic operations

## Basic (Addition, Subtraction, Multiplication, Division, Modulo)
```bash
a=5
b=6

res_add=$(($a + $b))
echo "$a + $b = $res_add"

res_sub=$(($a - $b))
echo "$a - $b = $res_sub"

res_multi=$(($a * $b))
echo "$a * $b = $res_multi"

res_div=$(($a / $b))
echo "$a / $b = $res_div"

res_mod=$(($a % $b))
echo "$a % $b = $res_mod"
```

<br/>

## Advanced (using _bc_)
```bash
a=5.2
b=2.25

# Division with decimal places accuracy
res_div=$(echo "scale=2; $a / $b" | bc)
echo $res_div

# Square root
res_sqrt=$(echo "scale=2;sqrt($a)" | bc )
echo $res_sqrt

# Power of
res_pow=$(echo "$a ^ $b" | bc)
echo $res_pow
```