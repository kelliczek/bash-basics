# Loops 

## For loops

```sh
for VARIABLE in 1 2 3 4 .. N;do  # {1..N} range
			# {1..N..2} 2=increment
	command1
	...
done
```

```sh
for VARIABLE in file1 file2 file3 file4 .. fileN;do
	command1
	...
done
```

```sh
for OUTPUT in $(command);do
	command1 on $OUTPUT
	...
done
```

```sh
for (( EXPRESION1; EXPRESION2; EXPRESION3 )); do
	command1
	...
done
```

### Using for loops to run commands
```sh
for command in command1 command2 ... commandN
do
	$command
done	
```

### Examples

```sh
NAMES = "John Doe Alice Mark"
for NAME in $NAMES #NAME == loop only local variable
do
	echo "Hello $NAME"
done
```

```sh
for i in {1..10}
do
	echo $i
done
```

```sh
# printing 1 to 10 with increment of 2
for i in {1..10..2}
do
	echo $i
done
```

```sh
for i in (( $i=0; i < 10; i++ ))
do
	echo $i
done
```

```sh
# renaming files
FILES = $(ls *.txt)
NEW = "new"
for FILE in $FILES
do
	echo "Renaming $FILE to new-$FILE"
	mv $FILE $NEW-$FILE
done
```

```sh
# iterating files
for item in * # * == current directory
do
	if [ -d $item] # printing only directories
	then
		echo $item
	fi
done
```

<br>


## Select loop

Gives menu structures and executes commands, often used with case statements

```sh
select VARIABLE in $array
do
	$echo "$VARIABLE selected"
done

# example with case
select name in mark john tom ben
do
	case $name in
		mark )
			echo "mark has been selected" ;;
		john )
			echo "john has been selected" ;;
		tom )
			echo "tom has been selected" ;;
		ben )
			echo "f u ben" ;;
		* )
			echo "error - unknown value"

	esac
done
```


<br>


## While loops

```sh
while [ condition ];do
	command1
	...
done
```


### Examples

```sh
# reading through file line by line
LINE = 1 #index | iterator
while read -r CURRENT_LINE
do
	echo "$LINE: $CURRENT_LINE"
	((LINE++))
done < "./file.txt"
```

```sh
# reading file
while read p
do
	echo $p
done < $FILENAME

# or
cat $FILENAME | while read p
do
	echo $p
done
```


<br>


## Until loops

```sh
until [ condition ];do # when condition is false, it will execute comands
	# reverse while loop
	command1
	...
done
```


<br>


## Break statement

Is used if you wanna break out of loop

```sh
for i in (( $i=0; i < 10; i++ ))
do
	if [ "$i" -eq 4 ]
	then
		break
	fi
done
```


<br>


## Continue statement

Is used if you wanna continue within a loop (opposite of break statement)

```sh
for i in (( $i=0; i < 10; i++ ))
do
	if [ "$i" -eq 4 ] || [ "$i" -eq 6 ]
	then
		continue
	fi
done
```
