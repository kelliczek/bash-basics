# Arrays

BASH only supports single dimensional array


## Declaring array

```sh
array_var = ('kali' 'arch' 'mint')
```

## Printing all elements

```sh
echo "${array_var[@]}"
```

## Printing specific element

```sh
echo "${array_var[i]}" #i from 0 to n-1
```

## Printing length of array

```sh
echo "${#array_var[@]}"
```

## Adding to array

```sh
array_var += newValue
```

## Removing element from array

```sh
unset array_var[i]
```