# User input

This section covers most common ways how to read user input using _read_ command.

## Reading text
```bash
echo "Enter a name:"
read name
echo "Hello! $name"
```
<br/>

## Reading from the same line
```bash
read -p "Enter a name: " name
echo "Hello! $name"
```
<br/>

## Reading password
```bash
read -sp "Enter secret key: " key
echo "Secret is $key"
```
<br/>

## Reading multiple variables
```bash
read -p "Enter a name, surname, age: " name surname age
echo "Human info: $name $surname $age"
```
<br/>

## Reading multiple variables using array
```bash
read -p "Enter a name, surname, age: " -a input_array
echo ${input_array[@]}
```