# Debugging scripts

To run script in debug mode	

```sh
bash -x SCRIPTNAME

# this will show you script work flow, similar to intermediate window
```

You can also debug from the script

```sh
set -x
echo "..."

commands...

set +x #will turn off debugging option
```