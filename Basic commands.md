# BASH - basic command list

Small collection of basic BASH commands with basic/common usage examples.

## General navigation

Very basic, yet essential commands for navigation through out the system 

* __`cd` change directory__
	* `$ cd /home/john/Documents` jumps to directory _/home/john/Documents_
	* `$ cd` or `$ cd ~` jumps to home directory
	* `$ cd ..` jumps back/up one directory
	* `$ cd /` jumps to root directory
	<br/>

* __`pwd` print working directory__
	* `$ pwd` prints (absolute) path that you are in
	<br/>

### Directory stack

Potentionally faster alternative to navigating in the system

* __`pushd` saves path to the stack__
	* `$ pushd /usr/bin/` saves _/usr/bin/_ to the stack
	<br/>

* __`popd` loads back saved path in stack; and removes it from the stack__
	* `$ popd` loads back top stack value; and removes it from the stack
	* `$ popd +1` loads second path from the stack; and removes it
	<br/>

* __`dirs` show paths in stack__
	* `$ dirs -l -v` lists all path in the stack
	<br/>

## Files & Directories

Commands to work with files and directories

* __`ls` list directory contents__
	* `$ ls` or `$ ls .` prints files and directories in current working directory
	* `$ ls /home/john/Downloads` prints files and directories in _/home/john/Downloads_ directory
	* `$ ls -l` prints files in long listing
	* `$ ls -a` prints hidden files
	* `$ ls -h` prints files and its sizes in human readable format
	* `$ ls *.jpeg` prints all _.jpeg_ files in current working directory
	<br/>

* __`tree` list directory contents in tree-like format__
	* `$ tree` or `$ tree .` prints files and directories in current working directory in tree-like format
	* `$ tree /home/john/Documents` prints files and directories in _/home/john/Documents_ in tree-like format
	* `$ tree -a` prints hidden files and directories
	* `$ tree -d` prints directories only
	<br/>

* __`cat` read file__
	* `$ cat file.txt` prints the content of _file.txt_
	* `$ cat -n file.txt` prints line numbers
	* `$ cat > new_file.txt` creates new empty file _new_file.txt_
	* `$ cat old_file > new_file` copies content of _old_file_ to _new_file_ (overwrites _new_file_)
	* `$ cat old_file >> new_file` copies content of _old_file_ to _new_file_ (appends to _new_file_)
	<br/>

* __`nano` command-line editor__
	* `$ nano` opens blank nano editor
	* `$ nano file.txt` opens _file.txt_ file in nano editor
	* `$ nano -l file.txt` opens file in nano editor with page lines
	<br/>

* __`touch` create new file__
	* `$ touch file.txt` creates new empty file _file.txt_
	* `$ touch -c file.txt` checks if the file _file.txt_ exists, if so it will not create it/overwrite it. If not, creates empty _file.txt_
	* `$ touch -a file.txt` updates the last access and modification times of _file.txt_ (sets it to current time and date)
	* `$ touch -m file.txt` updates the modification fime of _file.txt_ (sets it to current time and date)
	* `$ touch -c -t YYDDHHMM file.txt` explicitly sets access and modification time to provided time and date
	* `$ touch -t YYDDHHMM.SS file.txt` creates file with specified time (other than current time)
	<br/>

* __`cp` copy file/directory__
	* `$ cp /home/john/.bashrc /home/john/Documents/.bashrc.backup` copies _/home/john/.bashrc_ to _/home/john/Documents/.bashrc.backup_
	* `$ cp -f file.txt newfile.txt` or `$ cp --force file.txt newfile.txt` if an existing destination file cannot be opened, remove it and try again
	* `$ cp -i file.txt newfile.txt` or `$ cp --interactive file.txt newfile.txt` prompts before overwriting
	* `$ cp -r ~/Documents ~/Documents_Backup` or `$ cp --recursive ~/Documents ~/Documents_Backup` copies directories recursively
	<br/>

* __`mv` move file/directory / rename file/directory__
	* `$ mv john.jpg not_john.jpg` renames file _john.jpg_ to _not_john.jpg_
	* `$ mv -f /etc/resolv.conf /etc/resolv.conf.bak` or `$ mv --force /etc/resolv.conf /etc/resolv.conf.bak` overwrites files without prompting
	* `$ mv -i /etc/resolv.conf /etc/resolv.conf.bak` or `$ mv --interactive /etc/resolv.conf /etc/resolv.conf.bak` prompts before overwriting file
	<br/>

* __`rm` remove files or directories__
	* `$ rm file.txt` deletes _file.txt_
	* `$ rm -f file1 file2` or `$ rm -f file1 file2` deletes files without prompting 
	* `$ rm -i file1 file2` prompts before every removal
	* `$ rm -r ~/Books` or `$ rm --recursive ~/Books` deletes all files in _~/Books_ directorys
	* `$ rm -rf ~/Books` most common way of quick directory removal directory
	* `$ rm ~/Documents/*.txt` deletes all _.txt_ files in _~/Documents_ directory
	* `$ rm -rf / *` please don't, just don't even try that
	<br/>

* __`file` determinate file type__
	* `$ file secrets.data` returns file type of _secrets.data_ file
	* `$ file -f files_to_test.txt` tests multiple files written in the _files_to_test.txt_
	<br/>

* __`grep` outputs matching pattern__
	* `$ grep 'john' names.txt` searches any line that containes _john_ in _names.txt_
	* `$ grep -i 'john' names.txt` case-insensitive search for the word _john_ in _names.txt_
	* `$ grep -r 'john' ~/Documents` or `$ grep -R 'john' ~/Documents` recursive search; searches entire directory
	* `$ grep -c 'john' names.txt` searches and displays the total number of times _john_ appears in _names.txt_  
	* `$ grep -w 'John' names.txt` searches exact match of provided word
	* `$ cat names.txt | grep 'John'` using grep with pipe
	<br/>

* __`diff` compares difference between files__
	* `$ diff file1.txt file2.txt` compares files, lists differences
	<br/>

* __`wc` print newline, word, bytes count for file__
	* `$ wc file.txt` prints number of lines, number of words, number of bytes of _file.txt_
	* `$ wc -l file.txt` prints number of lines
	* `$ wc -w file.txt` prints number of words
	* `$ wc -m file.txt` prints number of characters
	* `$ wc -c file.txt` prints count number of bytes
	<br/>

* __`sort` sort contents of text file__
	* `$ sort names.txt` prints sorted lines alphabetically in _names.txt_
	* `$ sort -r names.txt` or `$ sort --reverse names.txt` prints reverse-order sorted lines
	* `$ sort -c names.txt` checks if _names.txt_ is sorted; if not, prints first unsorted line
	<br/>

* __`head` output the first part of file__
	* `$ head file.txt` prints first 10 lines in _file.txt_
	* `$ head -5 file.txt` or `$ head -n 5 file.txt` prints first 5 lines in file
	* `$ head -c 50 file.txt` prints first 50 characters in file 
	<br/>

* __`tail` output the last part of files__
	* `$ tail file.txt` prints last 10 lines in _file.txt_
	* `$ tail -n 5 file.txt` prints last 5 lines
	* `$ tail -f 15 file.txt` prints last 15 lines and continuously updates screen if new lines are written  
	<br/>

* __`more` display text; one screen at a time__
	* `$ more file.txt` prints content of _file.txt_ that fits on one screen
	* `$ more +5 file.txt` prints content of file beginning at line 5
	<br/>

* __`less` feature-rich command-line file viewer__
	* `$ less file.txt` views the _file.txt_ file
	* `$ less -N file.txt` views file and displays line numbers
	<br/>


### Compressing & Extracting files

Commands to work with archives

* __`unzip` extract ZIP archive__
	* `$ unzip files.zip` extracts contents of _files.zip_ archive
	* `$ unzip files.zip -d /home/john/Documents` extracts archive to _/home/john/Documents_ directory
	* `$ unzip -l files.zip` prints contents of _files.zip_ archive
	* `$ unzip -P s3cr3ts files.zip` extracts password-protected archive with password _s3cr3ts_
	<br/>

* __`zip` compress ZIP archive__
	* `$ zip files.zip file1 file2 file3` creates _files.zip_ archive of files _file1_, _file2_, _file3_
	* `$ zip -r files.zip ~/Books` creates archive from a directory _~/Books_ 
	* `$ zip files.zip file1 file2 -e` creates password-protected archive; prompts for password
	<br/>

* __`gunzip` extract GZ archive__
	* `$ gunzip files.gz` extracts contents of _files.gz_ archive
	<br/>

* __`gzip` compress GZ arhive__
	* `$ gzip -r ~/Books` creates .gz archive from _~/Books_ directory; deletes uncompressed files
	*	`$ gzip -k -r ~/Books` creates .gz archive; keeps uncompressed files
	<br/>

* __`tar` archive utility__
	* `$ tar -cvf files.tar file1 file2 file3` creates _files.tar_ archive of files _file1_, _file2_, _file3_
	* `$ tar -xvf files.tar` extracts contents of _files.tar_
	<br/>

### File verification (checksum)

Commands to check/verify file hashes

* __`md5sum` MD5 checksum__
	* `$ md5sum file.download` prints MD5 checksum of _file.download_ file
	<br/>

* __`sha1sum` SHA1 checksum__
	* `$ sha1sum file.download` prints SHA1 checksum of _file.download_ file
	<br/>

* __`sha224` SHA224 checksum__
	* `$ sha224sum file.download` prints SHA224 checksum of _file.download_ file
	<br/>

* __`sha256sum` SHA256 checksum__
	* `$ sha256sum file.download` prints SHA256 checksum of _file.download_ file
	<br/>

* __`sha384sum` SHA384 checksum__
	* `$ sha384sum file.download` prints SHA384 checksum of _file.download_ file
	<br/>

* __`sha512sum` SHA512 checksum__
	* `$ sha512sum file.download` prints SHA512 checksum of _file.download_ file
	<br/>

### File & Directory permissions

First you have to recognize file from a directory. When doing long listing of directory (`ls -l`), output on the left-side determinates whether it is a file or directory:

* file: __- rw- rw- r--__
* directory: __d rw- rw- r--__
<br/>

User (owner) | Group | Everyone else
------------- | ------------- | -------------
r w x | r w x | r w x
<br/>

Read (r) | Write (w) | Execute (x)
------------- | ------------- | -------------
__4__ | __2__ | __1__
<br/>

* __`chmod` change file mode bits__
	* `$ chmod 0777 file.txt` changes permission to __rwx rwx rwx__ to _file.txt_
	* `$ chmod 777 file.txt` same as above; changes permission to __rwx rwx rwx__ to _file.txt_
	* `$ chmod u=rwx g=rw o=r file.txt` changes permission to 0761 to _file.txt_
	* `$ chmod +r file.txt` adds __read__ permission to _file.txt_
	* `$ chmod +w file.txt` adds __write__ permission to _file.txt_
	* `$ chmod +x file.txt` adds __execution__ permission to _file.txt_
	* `$ chmod -R ~/Videos 0755` or `$ chmod --recursive ~/Videos 0755` changes files and directories recursively 
	<br/>

## System

Commands to display/get additional system functions

* __`echo` prints text__
	* `$ echo 'hello world'` prints _hello world_
	<br/>

* __`date` print or set date__
	* `$ date` prints current system's date and time
	* `$ date -s 28/12/2004 11:35:00` sets system date and time to December 28th, 2004, 11:35:00 AM
	<br/>

* __`sudo` super user do; grants root privileges__
	* `$ sudo rm -rf /etc/config/file.bak` removes _/etc/config/file.bak_ file as root
	**ALWAYS** double-triple check if you really want to run something as a root user; **can have catastrophic aftermath!**
	<br/>

* __`history` command history__
	* `$ history` prints last 1000 used commands
	* `$ history 5` prints 5 recent used commands
	* `$ !123` run the _123rd_ command used
	* `$ history | tail -n 5` prints 5 recent used commands
	* `$ history | head -n 5` prints 5 first used commands 
	<br/>

* __`uname` current system information__
	* `$ uname` prints operating system information
	* `$ uname -a` prints kernel name, kernel version, operating system hostname 
	<br/>

* __`watch` execute a program periodically, showing output__
	* `$ watch date` periodically refreshes command _date_
	* `$ watch -n 5 date` periodically refreshes command _date_ every 5 seconds
	* `$ watch -d date` periodically refreshes and highlights changes
	<br/>

* __`sleep` sleep__*
	* `$ sleep 8` # sleep for 8 seconds

### Manual Pages

Commands for showing manual pages or short descriptions

* __`info` read info documents__
	* `$ info --subnodes` lists menu items
	* `$ info cd` lists info page of _cd_ command
* __`which` show full command path__
	* `$ which cd` prints full path of _cd_ command
	<br/>

* __`man` manual page__
	* `$ man cd` ouputs manual page of _cd_ command
	<br/>

* __`whatis` one-line manual page description__
	* `$ whatis cd` prints short sentence of _cd_ command 
	<br/>


### Searching in the system

Commands to locate files, directories or commands in the system

* __`locate` search in system__
	* `$ locate joker` searches all instances of _joker_ in the system; case sensitive
	* `$ locate -i batman` searches all instances of _batman_ in the system; case in-sensitive
	<br/>

* __`find` search in system__
	* `$ find ~/Documents -name file.txt` prints all files whose name is _file.txt_ in _~/Documents_ directory
	* `$ find . -iname file.txt` case-insensitive search in current working directory of _file.txt_ file
	* `$ find / -type d -iname Books` case-insensitive search for directory _Books_ in root directory
	* `$ find / -type f -iname *.txt` searches all _.txt_ files in root directory 
	<br/>

### Disk devices

Commands to display various information about disk devices in the system

* __`df` report file system disk space usage__
	* `$ df` displays the information of device name, total blocks, total disk space, used disk space, available disk space and mount points on a file system
	* `$ df -h` same as above but in human readable format
	* `$ df -i` displays the number of used inodes and their percentage for the file system
	* `$ df -T` displays file system type
	<br/>

* __`du` disk usage__
	* `$ du ~/Books` displays number of disk blocks in the _~/Books_ along with its subdirectories
	* `$ du -h ~/Books` displays in human readable format
	* `$ du -s ~/Books` displays only sumary of _~/Books_ directory
	<br/>

* __`lsblk` list block devices__
	* `$ lblks` displays block devices
	* `$ lsblk -a` displays empty devices as well
	<br/>

### Network devices

Commands to display network interfaces and their configurations or throubleshooting

* __`ping` send ICMP request__
	* `$ ping hello.world.xyz` sends continuously ping requests to host _hello.world.xyz_ 
	* `$ ping -c 4 hello.world.xyz` sends 4 ping requests
	* `$ ping -i 5 hello.world.xyz` sends ping requests in 5-seconds intervals
	<br/>

* __`ifconfig` configure network interface__
	* `$ ifconfig` displays information about all network interfaces currently active
	* `$ ifconfig -a` displays information about all network interfaces in the system (active + inactive)
	* `$ ifconfig eth0` displays information about network interface _eth0_
	* `$ ifconfig eth0 down` disables network interface _eth0_
	* `$ ifconfig eth0 up` enables network interface _eth0_
	<br/>

* __`traceroute` packet route trace__
	* `traceroute hello.world.xyz` displays number of hops taken to reach destination address _hello.world.xyz_ (and travelling path)
	* `traceroute -m 20 hello.world.xyz` sets max TTL to 20 hops 
	<br/>


## Administrative

Commands for basic system maintenance 

### Users & Groups

* __`passwd` changes password__
	* `$ passwd` changes password of logged in user
	* `$ passwd john` changes password for _john_
	* `$ passwd -d john` delete pasword for _john_; makes it empty
	* `$ passwd -e john` expires password for _john_; next time _john_ logs in, he is prompted to change password
	<br/>

* __`users` print logged in users__
	* `$ users` prints currently logged in users  
	<br/>

* __`groups` print group names__
	* `$ groups` prints group names for the logged in user
	<br/>

* __`id` print user and group IDs__
	* `$ id` prints username, user ID, user groups, group IDs of logged in user
	* `$ id -u john` prints username, user ID, user groups, group IDs of user _john_
	<br/>

* __`adduser / useradd` create new user or update new user information__
	* `$ useradd john` creates new user _john_
	*	`$ useradd -m john` creates new user _john_ and hist home directory
	* `$ useradd -G wheel,sys,network` add user _john_ to groups _wheel_, _sys_ and _network_
	<br/>

* __`su` switch user or run command as other user__
	* `$ sudo su` switches to root user
	Be **VERY** careful with this command
	* `$ su john` switches to user _john_
	* `$ su - john` switches to user _john_ and to his home directtory
	* `su john -C ls -al` run command _ls -al_ as user _john_
	<br/>

* __`deluser / userdel` remove user__
	* `$ deluser john` deletes user _john_
	* `$ deluser --remove-home john` deletes _john_ and his home directory
	<br/>

* __`addgroup / groupadd` create new group__
	* `$ groupadd sales` adds new group named _sales_
	<br/>

* __`delgroup / groupdel` delete group__
	* `$ delgroup sales` deletes group named _sales_
	<br/>


* __`usermod` modify user account__
	* `$ sudo usermod -a -G sales john` adds user _john_ to _sales_ group
	<br/>

* __`chown` change owner__
	* `$ sudo chown john file.txt` changes owner of the _file.txt_ file to user _john_
	* `$ sudo chown john:sales file.txt` changes owner to _john_ and group _sales_
	* `$ sudo chown -R :sales ~/SALES` changes owner of the directory _~/SALES_ to group _sales_
	<br/>

#### Using Access Control List

Much easier and simpler way of managing groups and permissions

* __`setfacl` set file access control list__
	* `$ sudo setfacl -m g:sales:rx -R /DATA` sets permissions to _rx_ for the group _sales_ in the _/DATA_ folder and all the contents in it (recursive) 
	* `$ sudo serfacl -m u:john:rwx -R /DATA` sets permissions to _rwx_ for the user _john_ in the _/DATA_ folder and all the contents in it (recursive)
	<br/>

### Processes

* __`top` display processes__
	* `$ top` displays system information and current running processes
	Use <kbd>↑</kbd>, <kbd>↓</kbd> or <kbd>Page Up</kbd>, <kbd>Page Down</kbd> keys to browse through the list
	Use <kbd>⇧n</kbd> to sort by PID
	Use <kbd>⇧c</kbd> to sort by CPU usage
	Use <kbd>⇧m</kbd> to sort by memory usage
	Use <kbd>q</kbd> to exit
	<br/>

* __`htop` interactive process viewer__
	* `$ htop` display system information and current running processes
	Use <kbd>↑</kbd>, <kbd>↓</kbd> or <kbd>Page Up</kbd>, <kbd>Page Down</kbd> keys to browse through the list
	Use <kbd>q</kbd> to exit
	<br/>

* __`ps` snapshot of the current processes__
	* `$ ps` displays running processes by the user running in terminal window
	* `$ ps axu` display all running processes and their owner
	<br/>

* __`<command> &` run process in the background__
	* `$ cp large.file /home/john/Documents &` runs _cp_ on the background; this way you can still interact with terminal
	<br/>

* __`jobs` display status of jobs in the current session__ 
	* `$ jobs` prints jobs and status in current session
	* `$ jobs -r` prints only running jobs
	* `$ jobs -s` prints only stopped jobs
	<br/>

* __`fg` run jobs in the foreground__
	* `$ fg %4` brings 4th job to the foreground
	* `$ fg %cp` brings _cp_ job to the foreground
	<br/>

* __`bg` run jobs in the background__
 * `$ bg %4` brings 4th job to the background
	<br/>

* __`kill` terminate a process__
	* `$ kill 4044` terminates process with PID _4044_
	* `$ kill -15 4044` be nice approach to killing process
	* `$ kill -9 4044` really terminates process
	* `$ kill -STOP 4044` stops process with PID _4044_
	* `$ kill -CONT 4044` resumes stopped process
	<br/>

* __`killall` terminate parrent proccess__
	* `$ killall firefox` terminates parrent proccess _firefox_ 
	<br/>

### SSH

* __`ssh` remote login__ 
	* `$ ssh john@192.168.1.1` login as user _john_ to host _192.168.1.1_
	<br/>

* __`ssh-keygen` uthentication key generation__
	* `$ ssh-keygen -t rsa -b 4096` generates RSA 4096
	<br/>

* __`ssh-copy-id` use locally available keys to authorise logins on a remote machine__
	* `$ ssh-copy-id -i ~/.ssh/key.pub john@192.168.1.1` copies public key to the remote server
	<br/>

* __`scp` secure copy__
	* `$ scp ~/Files/file.txt john@192.168.1.1:/home/john/Documents` uploads _~/Files/file.txt_ file to remote server to the _/home/john/Documents_ directory
	* `$ scp john@192.168.1.1:/home/john/file.txt ~/Files` downloads _/home/john/file.txt_ file from the remote server to the _~/Files_ directory
	<br/>

## Shortcuts / Panic situations

* `ctrl + c` stops the current running command on the foreground
* `ctrl + d` used when asked for an input, with this shortcut you end the input phase 
* `ctrl + z` pause current job
<br/>

## Pipe

Bash provides command line interface facility which mainly used to concatenate command output to another command

* __`|` pipe__
	* `$ cat names.txt | sort` returns content of _names.txt_ sorted
	* `$ cat names.txt | grep 'john'` returns content of _names.txt_ with lines containing _john_
	* `$ cat names.txt | less` returns content of _names.txt_ with scrolling option
	* `$ cat zoo.txt | grep -i 'donkey' | sort` returns content of _zoo.txt_ that contains _donkey_ and is sorted
	<br/>

## Input, Output, Error redirection

Redirection is a feature in Linux such that when executing a command, you can change the standard input/output devices. The basic workflow of any Linux command is that it takes an input and give an output.

* standard input (stdin) device is the keyboard
* standard output (stdout) device is the screen

### Input redirection

* __`<`__
	* `$ cat < /etc/resolv.conf` reads the content of _resolv.conf_
	<br/>

* __`<<`__
	* `cat << vpn.conf ` ask user for inputs to be written to the temporarily created file; gets saved once typed _vpn.conf_
	<br/>

* __`<<<` less common way of input redirection__
	* `$ cat <<< 'hello world'` outputs _hello world_
	<br/>

### Output redirection

* __`>` overwrite__
	* `$ echo "hello world" > file.txt` overwrites _file.txt_ and writes _hello world_; if _file.txt_ does not exist, it will create it
	* `$ ifconfig eth0 > /home/joh/network` outputs network configuration of _eth0_ to _/home/john/network_
	<br/>

* __`>>` append__
	* `$ cat dhclient.conf >> dhclient.conf.old` appends the content of _dhclient.conf_ to _dhclient.conf.old_; if _dhclient.conf.old_ does not exist, it will create it 
	* `$ echo "alias ll='ls -al'" >> /home/john/.bashrc` appends _alias ll='ls -al'_ to _.bashrc_; if _.bashrc_ does not exist, it will create it
	<br/>

### Error redirection

File | File Descriptor
------------- | -------------
Standard Input __STDIN__ | __0__
Standard Output __STDOUT__  | __1__
Standard Error __STDERR__ | __2__
<br/>

__NOTE:__ File Descriptor is identified by a unique non-negative integer; at least one file descriptor exists for every open file on the system

* __`2>` error redirection__
	* `$ ./setup.sh 2> err.txt` executes _./setup.sh_, any errors during run time will be written to _err.txt_
	<br/>

* __`2>&1` writes _STDOUT_ and _STDERR_ to the same file__
	* `$ ls /proc/ /mem/ > system.txt 2>&1` writes the output from one file to the input of another file; we are redirecting error output to standard output which in turn is being re-directed to file _system.txt_; hence, both the output is written to _system.txt_
	<br/>

